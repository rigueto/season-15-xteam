3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000
4         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL
0         5         10000         4             32            1    1    1    1    1    1    1    1
1         6         10000         4             32            1    1    1    1    1    1    1    1
2         7         10000         4             32            1    1    1    1    1    1    1    1
3         8         10000         4             32            1    1    1    1    1    1    1    1
4         9         10000         4             32            1    1    1    1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
6159      0       0       *         *         *         *         *         *         *         0      //Jewel of Chaos
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
7182      0       0       *         *         *         *         *         *         *         0      //Jewel of Soul
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
7184      0       0       *         *         *         *         *         *         *         0      //Jewel of Life
end

8
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
7181      0       0       *         *         *         *         *         *         *         0      //Jewel of Bless
end

9
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment    
6268      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6269      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6270      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6271      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6272      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6273      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)
end