3
//Index   DropRate
0         10000
end
4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL
0         5         9500          0	            0             1    1    1    1    1    1    1    1    1    1    
0         6         500           0	            0             1    1    1    1    1    1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration     	Comment
// Arma's Box of Kundun +4
0016	  0       1       *         12        13        20        602       *         *         0         		//Sword of Destruction Sword
0017      0       1       *         12        13        20        602       *         *         0         		//Dark Breaker Sword
0018      0       1       *         12        13        20        602       *         *         0         		//Thunder Blade
0031	  0       1       *         12        13        20        602       *         *         0         		//Rune Blade
0032      0       1       *         12        13        20        602       *         *         0               //Sacred Glove
1029      0       1       *         12        13        20        602       *         *         0         		//Crystal Sword
1035      0       1       *         12        13        20        602       *         *         0         		//Lord Scepter
1545      0       1       *         12        13        20        602       *         *         0         		//Bill of Balrog
2064      0       1       *         12        13        20        602       *         *         0         		//Saint Crossbow
2065      0       1       *         12        13        20        602       *         *         0         		//Celestial Bow
2569      0       1       *         12        13        20        602       *         *         0         		//Dragon Soul Staff
2577      0       1       *         12        13        20        602       *         *         0         		//Ancient Staff
3087      0       1       *         12        13        20        602       *         *         0         		//Grand Soul Shield
3085      0       1       *         12        13        20        602       *         *         0         		//Dragon Shield
0067      0       1       *         12        13        20        602       *         *         0         		//Dacia Short Sword
1052      0       1       *         12        13        20        602       *         *         0         		//Elemental Rune Mace

//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Black Dragon (dk)
3600      0       1       *         *         13        20        602       *         *         0          		//Helm Black Dragon
4112      0       1       *         *         13        20        602       *         *         0          		//Armo Black Dragon
4624      0       1       *         *         13        20        602       *         *         0          		//Pants Black Dragon
5136      0       1       *         *         13        20        602       *         *         0          		//Gloves Black Dragon
5648      0       1       *         *         13        20        602       *         *         0          		//Boots Black Dragon
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Dark Phoenix (dk)
3601      0       1       *         *         13        20        602       *         *         0          		//Helm Dark Phoenix
4113      0       1       *         *         13        20        602       *         *         0          		//Armo Dark Phoenix
4625      0       1       *         *         13        20        602       *         *         0          		//Pants Dark Phoenix
5137      0       1       *         *         13        20        602       *         *         0          		//Gloves Dark Phoenix
5649      0       1       *         *         13        20        602       *         *         0          		//Boots Dark Phoenix
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Grand Soul (dw)
3602      0       1       *         *         13        20        602       *         *         0          		//Helm Grand Soul
4114      0       1       *         *         13        20        602       *         *         0          		//Armo Grand Soul
4626      0       1       *         *         13        20        602       *         *         0          		//Pants Grand Soul
5135      0       1       *         *         13        20        602       *         *         0          		//Gloves Grand Soul
5650      0       1       *         *         13        20        602       *         *         0          		//Boots Grand Soul
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Holy Spirit(elf)
3603      0       1       *         *         13        20        602       *         *         0          		//Helm Holy Spirit
4115      0       1       *         *         13        20        602       *         *         0          		//Armo Holy Spirit
4627      0       1       *         *         13        20        602       *         *         0          		//Pants Holy Spirit
5139      0       1       *         *         13        20        602       *         *         0          		//Gloves Holy Spirit
5651      0       1       *         *         13        20        602       *         *         0          		//Boots Holy Spirit
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Thunder Hawk(mg)
4116      0       1       *         *         13        20        602       *         *         0          		//Armo Thunder Hawk
4628      0       1       *         *         13        20        602       *         *         0          		//Pants Thunder Hawk
5140      0       1       *         *         13        20        602       *         *         0          		//Gloves Thunder Hawk
5652      0       1       *         *         13        20        602       *         *         0          		//Boots Thunder Hawk
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Dark Stell (dl)
3611      0       1       *         *         13        20        602       *         *         0          		//Helm Dark Stell
4123      0       1       *         *         13        20        602       *         *         0          		//Armo Dark Stell
4635      0       1       *         *         13        20        602       *         *         0          		//Pants Dark Stell
5147      0       1       *         *         13        20        602       *         *         0          		//Gloves Dark Stell
5659      0       1       *         *         13        20        602       *         *         0          		//Boots Dark Stell
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Ancient (Sum)
3625      0       1       *         *         13        20        602       *         *         0          		//
4137      0       1       *         *         13        20        602       *         *         0          		//
4649      0       1       *         *         13        20        602       *         *         0          		//
5161      0       1       *         *         13        20        602       *         *         0          		//
5673      0       1       *         *         13        20        602       *         *         0          		//
// Set Red Wing (Sum)
3624      0       1       *         *         13        20        602       *         *         0          		//
4136      0       1       *         *         13        20        602       *         *         0          		//
4648      0       1       *         *         13        20        602       *         *         0          		//
5160      0       1       *         *         13        20        602       *         *         0          		//
5672      0       1       *         *         13        20        602       *         *         0          		//
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Storm Jahad (RF)
3644      0       1       *         *         13        20        602       *         *         0          		//
4156      0       1       *         *         13        20        602       *         *         0          		//
4668      0       1       *         *         13        20        602       *         *         0          		//
5692      0       1       *         *         13        20        602       *         *         0          		//
//Sacred Fire (RF)
3643      0       1       *         *         13        20        602       *         *         0          		//
4155      0       1       *         *         13        20        602       *         *         0          		//
4667      0       1       *         *         13        20        602       *         *         0          		//
5691      0       1       *         *         13        20        602       *         *         0          		//
//Set Legendary RM
3702      0       1       *         *         13        20        602       *         *         0         		//
4214      0       1       *         *         13        20        602       *         *         0         		//
4726      0       1       *         *         13        20        602       *         *         0         		//
5238      0       1       *         *         13        20        602       *         *         0         		//
5633      0       1       *         *         13        20        602       *         *         0         		//
//Rune Spink Mask RM
3701      0       1       *         *         13        20        602       *         *         0         		//
4213      0       1       *         *         13        20        602       *         *         0         		//
4725      0       1       *         *         13        20        602       *         *         0         		//
5237      0       1       *         *         13        20        602       *         *         0         		//
5749      0       1       *         *         13        20        602       *         *         0         		//
//Set Slayer
3759      0       1       *         *         13        20        602       *         *         0         		//
4271      0       1       *         *         13        20        602       *         *         0         		//
4783      0       1       *         *         13        20        602       *         *         0         		//
5295      0       1       *         *         13        20        602       *         *         0         		//
5807      0       1       *         *         13        20        602       *         *         0         		//
//Set Slayer
3758      0       1       *         *         13        20        602       *         *         0         		//
4270      0       1       *         *         13        20        602       *         *         0         		//
4782      0       1       *         *         13        20        602       *         *         0         		//
5294      0       1       *         *         13        20        602       *         *         0         		//
5806      0       1       *         *         13        20        602       *         *         0         		//

end


6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration     	Comment
// Arma's Box of Kundun +4
0016	  0       1       *         12        13        20        602       *         *         0         		//Sword of Destruction Sword
0017      0       1       *         12        13        20        602       *         *         0         		//Dark Breaker Sword
0018      0       1       *         12        13        20        602       *         *         0         		//Thunder Blade
0031	  0       1       *         12        13        20        602       *         *         0         		//Rune Blade
0032      0       1       *         12        13        20        602       *         *         0               //Sacred Glove
1029      0       1       *         12        13        20        602       *         *         0         		//Crystal Sword
1035      0       1       *         12        13        20        602       *         *         0         		//Lord Scepter
1545      0       1       *         12        13        20        602       *         *         0         		//Bill of Balrog
2064      0       1       *         12        13        20        602       *         *         0         		//Saint Crossbow
2065      0       1       *         12        13        20        602       *         *         0         		//Celestial Bow
2569      0       1       *         12        13        20        602       *         *         0         		//Dragon Soul Staff
2577      0       1       *         12        13        20        602       *         *         0         		//Ancient Staff
3087      0       1       *         12        13        20        602       *         *         0         		//Grand Soul Shield
3085      0       1       *         12        13        20        602       *         *         0         		//Dragon Shield
0067      0       1       *         12        13        20        602       *         *         0         		//Dacia Short Sword
1052      0       1       *         12        13        20        602       *         *         0         		//Elemental Rune Mace

//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Black Dragon (dk)
3600      0       1       *         *         13        20        602       *         *         0          		//Helm Black Dragon
4112      0       1       *         *         13        20        602       *         *         0          		//Armo Black Dragon
4624      0       1       *         *         13        20        602       *         *         0          		//Pants Black Dragon
5136      0       1       *         *         13        20        602       *         *         0          		//Gloves Black Dragon
5648      0       1       *         *         13        20        602       *         *         0          		//Boots Black Dragon
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Dark Phoenix (dk)
3601      0       1       *         *         13        20        602       *         *         0          		//Helm Dark Phoenix
4113      0       1       *         *         13        20        602       *         *         0          		//Armo Dark Phoenix
4625      0       1       *         *         13        20        602       *         *         0          		//Pants Dark Phoenix
5137      0       1       *         *         13        20        602       *         *         0          		//Gloves Dark Phoenix
5649      0       1       *         *         13        20        602       *         *         0          		//Boots Dark Phoenix
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Grand Soul (dw)
3602      0       1       *         *         13        20        602       *         *         0          		//Helm Grand Soul
4114      0       1       *         *         13        20        602       *         *         0          		//Armo Grand Soul
4626      0       1       *         *         13        20        602       *         *         0          		//Pants Grand Soul
5135      0       1       *         *         13        20        602       *         *         0          		//Gloves Grand Soul
5650      0       1       *         *         13        20        602       *         *         0          		//Boots Grand Soul
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Holy Spirit(elf)
3603      0       1       *         *         13        20        602       *         *         0          		//Helm Holy Spirit
4115      0       1       *         *         13        20        602       *         *         0          		//Armo Holy Spirit
4627      0       1       *         *         13        20        602       *         *         0          		//Pants Holy Spirit
5139      0       1       *         *         13        20        602       *         *         0          		//Gloves Holy Spirit
5651      0       1       *         *         13        20        602       *         *         0          		//Boots Holy Spirit
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Thunder Hawk(mg)
4116      0       1       *         *         13        20        602       *         *         0          		//Armo Thunder Hawk
4628      0       1       *         *         13        20        602       *         *         0          		//Pants Thunder Hawk
5140      0       1       *         *         13        20        602       *         *         0          		//Gloves Thunder Hawk
5652      0       1       *         *         13        20        602       *         *         0          		//Boots Thunder Hawk
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Dark Stell (dl)
3611      0       1       *         *         13        20        602       *         *         0          		//Helm Dark Stell
4123      0       1       *         *         13        20        602       *         *         0          		//Armo Dark Stell
4635      0       1       *         *         13        20        602       *         *         0          		//Pants Dark Stell
5147      0       1       *         *         13        20        602       *         *         0          		//Gloves Dark Stell
5659      0       1       *         *         13        20        602       *         *         0          		//Boots Dark Stell
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Ancient (Sum)
3625      0       1       *         *         13        20        602       *         *         0          		//Helm Ancient
4137      0       1       *         *         13        20        602       *         *         0          		//Armo Ancient
4649      0       1       *         *         13        20        602       *         *         0          		//Pants Ancient
5161      0       1       *         *         13        20        602       *         *         0          		//Gloves Ancient
5673      0       1       *         *         13        20        602       *         *         0          		//Boots Ancient
// Set Red Wing (Sum)
3624      0       1       *         *         13        20        602       *         *         0          		//
4136      0       1       *         *         13        20        602       *         *         0          		//
4648      0       1       *         *         13        20        602       *         *         0          		//
5160      0       1       *         *         13        20        602       *         *         0          		//
5672      0       1       *         *         13        20        602       *         *         0          		//
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Storm Jahad (RF)
3644      0       1       *         *         13        20        602       *         *         0          		//
4156      0       1       *         *         13        20        602       *         *         0          		//
4668      0       1       *         *         13        20        602       *         *         0          		//
5692      0       1       *         *         13        20        602       *         *         0          		//
//Sacred Fire (RF)
3643      0       1       *         *         13        20        602       *         *         0          		//
4155      0       1       *         *         13        20        602       *         *         0          		//
4667      0       1       *         *         13        20        602       *         *         0          		//
5691      0       1       *         *         13        20        602       *         *         0          		//
//Set Legendary RM
3702      0       1       *         *         13        20        602       *         *         0         		//
4214      0       1       *         *         13        20        602       *         *         0         		//
4726      0       1       *         *         13        20        602       *         *         0         		//
5238      0       1       *         *         13        20        602       *         *         0         		//
5633      0       1       *         *         13        20        602       *         *         0         		//
//Rune Spink Mask RM
3701      0       1       *         *         13        20        602       *         *         0         		//
4213      0       1       *         *         13        20        602       *         *         0         		//
4725      0       1       *         *         13        20        602       *         *         0         		//
5237      0       1       *         *         13        20        602       *         *         0         		//
5749      0       1       *         *         13        20        602       *         *         0         		//
//Set Slayer
3759      0       1       *         *         13        20        602       *         *         0         		//
4271      0       1       *         *         13        20        602       *         *         0         		//
4783      0       1       *         *         13        20        602       *         *         0         		//
5295      0       1       *         *         13        20        602       *         *         0         		//
5807      0       1       *         *         13        20        602       *         *         0         		//
//Set Slayer
3758      0       1       *         *         13        20        602       *         *         0         		//
4270      0       1       *         *         13        20        602       *         *         0         		//
4782      0       1       *         *         13        20        602       *         *         0         		//
5294      0       1       *         *         13        20        602       *         *         0         		//
5806      0       1       *         *         13        20        602       *         *         0         		//
end