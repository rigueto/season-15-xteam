3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL
0         5         10000         100           32           1    1    1    1    1    1    1    1    1    1
1         6         10000         100           32           1    1    1    1    1    1    1    1    1    1
2         7         10000         0             4            1    1    1    1    1    1    1    1    1    1
3         8         10000         100           32           1    1    1    1    1    1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6467      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"    
6468      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"   
6469      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"     
6470      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"    
6471      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)
6472      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"   
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7513      0       0       *         *         *         *         *         *         *         0          //Mysterious Stone
7635      0       0       *         *         *         *         *         *         *         0          //Guardian Enhanced Stone
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7124      0       0       *         *         *         *         *         *         *         0          //Golden Sentence
7124      0       0       *         *         *         *         *         *         *         0          //Golden Sentence
7124      0       0       *         *         *         *         *         *         *         0          //Golden Sentence
7124      0       0       *         *         *         *         *         *         *         0          //Golden Sentence
7124      0       0       *         *         *         *         *         *         *         0          //Golden Sentence
end

8
//MuGrind Coin
7288      0       0       -1        -1        -1        -1        -1        -1         -1         0       //MuGrind Coin
end