3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000
4         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL
0         5         10000         3500          16            1    1    1    1    1    1	1    1    1    1
1         6         10000         100           32            1    1    1    1    1    1    1    1    1    1 
2         7         10000         100           32            1    1    1    1    1    1	1    1    1    1
3         8         10000         100           32            1    1    1    1    1    1	1    1    1    1
4         9         10000         0             4             1    1    1    1    1    1	1    1    1    1
end


5
//secao para ruud direto inventorio
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6352        0       0       *         *         *         *         *        *         5         0      //Kundun's Madness Blade"      
6353        0       0       *         *         *         *         *        *         5         0      //Kundun's Magic Spell Scroll" 
6354        0       0       *         *         *         *         *        *         5         0      //Empire Guardians' Stronghold"
6355        0       0       *         *         *         *         *        *         5         0      //Ancient Icarus Scroll"       
6356        0       0       *         *         *         *         *        *         5         0      //Arca's Prophecy"             
6357        0       0       *         *         *         *         *        *         5         0      //Antonia's Sword"             
6358        0       0       *         *         *         *         *        *         5         0      //Kundun's Seal Scroll"        
6450        0       0       *         *         *         *         *        *         5         0      //Runedil's Goldentune Harp" 
6451        0       0       *         *         *         *         *        *         5         0      //Lemuria's Orb"             
6452        0       0       *         *         *         *         *        *         5         0      //Norrwen's Bloodstring Lyra"
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Bloodangel Slayer   
3764        0       1       *         *         13        20        602       *         *         0	
4276        0       1       *         *         13        20        602       *         *         0	
4788        0       1       *         *         13        20        602       *         *         0	
5300        0       1       *         *         13        20        602       *         *         0	
5812        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Runist  
3706        0       1       *         *         13        20        602       *         *         0	
4218        0       1       *         *         13        20        602       *         *         0	
4730        0       1       *         *         13        20        602       *         *         0	
5242        0       1       *         *         13        20        602       *         *         0	
5754        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Knight   
3682        0       1       *         *         13        20        602       *         *         0	
4194        0       1       *         *         13        20        602       *         *         0	
4706        0       1       *         *         13        20        602       *         *         0	
5218        0       1       *         *         13        20        602       *         *         0	
5730        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Wizard   
3683        0       1       *         *         13        20        602       *         *         0	
4195        0       1       *         *         13        20        602       *         *         0	
4707        0       1       *         *         13        20        602       *         *         0	
5219        0       1       *         *         13        20        602       *         *         0	
5731        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Elf (A)
3684        0       1       *         *         13        20        602       *         *         0	
4196        0       1       *         *         13        20        602       *         *         0	
4708        0       1       *         *         13        20        602       *         *         0	
5220        0       1       *         *         13        20        602       *         *         0	
5732        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Summoner 
3685        0       1       *         *         13        20        602       *         *         0	
4197        0       1       *         *         13        20        602       *         *         0	
4709        0       1       *         *         13        20        602       *         *         0	
5221        0       1       *         *         13        20        602       *         *         0	
5733        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Magic (A)
3686        0       1       *         *         13        20        602       *         *         0	
4198        0       1       *         *         13        20        602       *         *         0	
4710        0       1       *         *         13        20        602       *         *         0	
5222        0       1       *         *         13        20        602       *         *         0	
5734        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Lord     
3687        0       1       *         *         13        20        602       *         *         0	
4199        0       1       *         *         13        20        602       *         *         0	
4711        0       1       *         *         13        20        602       *         *         0	
5223        0       1       *         *         13        20        602       *         *         0	
5735        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Fighter  
3688        0       1       *         *         13        20        602       *         *         0	
4200        0       1       *         *         13        20        602       *         *         0	
4712        0       1       *         *         13        20        602       *         *         0	
5736        0       1       *         *         13        20        602       *         *         0	
//Bloodangel Lancer   
3689        0       1       *         *         13        20        602       *         *         0	
4201        0       1       *         *         13        20        602       *         *         0	
4713        0       1       *         *         13        20        602       *         *         0	
5225        0       1       *         *         13        20        602       *         *         0	
5737        0       1       *         *         13        20        602       *         *         0	
 

//Darkangel Knight     
3722        0       1       *         *         13        20        602       *         *         0		
4234        0       1       *         *         13        20        602       *         *         0		
4746        0       1       *         *         13        20        602       *         *         0		
5258        0       1       *         *         13        20        602       *         *         0		
5770        0       1       *         *         13        20        602       *         *         0		
//Darkangel Wizard     
3723        0       1       *         *         13        20        602       *         *         0		
4235        0       1       *         *         13        20        602       *         *         0		
4747        0       1       *         *         13        20        602       *         *         0		
5259        0       1       *         *         13        20        602       *         *         0		
5771        0       1       *         *         13        20        602       *         *         0		
//Darkangel Elf (A)   
3724        0       1       *         *         13        20        602       *         *         0		
4236        0       1       *         *         13        20        602       *         *         0		
4748        0       1       *         *         13        20        602       *         *         0		
5260        0       1       *         *         13        20        602       *         *         0		
5772        0       1       *         *         13        20        602       *         *         0		
//Darkangel Summoner   
3725        0       1       *         *         13        20        602       *         *         0		
4237        0       1       *         *         13        20        602       *         *         0		
4749        0       1       *         *         13        20        602       *         *         0		
5261        0       1       *         *         13        20        602       *         *         0		
5773        0       1       *         *         13        20        602       *         *         0		
//Darkangel Magic (A)
3726        0       1       *         *         13        20        602       *         *         0		
4238        0       1       *         *         13        20        602       *         *         0		
4750        0       1       *         *         13        20        602       *         *         0		
5262        0       1       *         *         13        20        602       *         *         0		
5774        0       1       *         *         13        20        602       *         *         0		
//Darkangel Lord       
3727        0       1       *         *         13        20        602       *         *         0		
4239        0       1       *         *         13        20        602       *         *         0		
4751        0       1       *         *         13        20        602       *         *         0		
5263        0       1       *         *         13        20        602       *         *         0		
5775        0       1       *         *         13        20        602       *         *         0		
//Darkangel Fighter    
3728        0       1       *         *         13        20        602       *         *         0		
4240        0       1       *         *         13        20        602       *         *         0		
4752        0       1       *         *         13        20        602       *         *         0		
5776        0       1       *         *         13        20        602       *         *         0		
//Darkangel Lancer     
3729        0       1       *         *         13        20        602       *         *         0		
4241        0       1       *         *         13        20        602       *         *         0		
4753        0       1       *         *         13        20        602       *         *         0		
5265        0       1       *         *         13        20        602       *         *         0		
5777        0       1       *         *         13        20        602       *         *         0		
//Darkangel Slayer
3765        0       1       *         *         13        20        602       *         *         0		
4277        0       1       *         *         13        20        602       *         *         0		
4789        0       1       *         *         13        20        602       *         *         0		
5301        0       1       *         *         13        20        602       *         *         0		
5813        0       1       *         *         13        20        602       *         *         0		
//Darkangel Runist 
3707        0       1       *         *         13        20        602       *         *         0		
4219        0       1       *         *         13        20        602       *         *         0		
4731        0       1       *         *         13        20        602       *         *         0		
5243        0       1       *         *         13        20        602       *         *         0		
5755        0       1       *         *         13        20        602       *         *         0		
end	

8
6244      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6245      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6246      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6247      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6248      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6249      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"    
6250      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6251      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6252      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6253      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6254      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6255      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"   
6256      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6257      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6258      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6259      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6260      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6261      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)""    
6262      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6263      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6264      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6265      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6266      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6267      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"    
6268      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6269      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6270      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6271      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6272      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6273      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)

end

9
//MuGrind Coin
7288      0       0       -1        -1        -1        -1        -1        -1         -1         0       //MuGrind Coin
7288      0       0       -1        -1        -1        -1        -1        -1         -1         0       //MuGrind Coin
7288      0       0       -1        -1        -1        -1        -1        -1         -1         0       //MuGrind Coin
end