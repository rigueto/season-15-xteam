3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000
4         10000
5         10000
6         10000
7         10000
8         10000
9         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL
0         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
1         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
2         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
3         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
4         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
5         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
6         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
7         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
8         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
9         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment               
14,003    0       30      *         *         *         *         *         *         *         *             //Large Healing Potion 
14,003    0       40      *         *         *         *         *         *         *         *             //Large Healing Potion 
14,003    0       50      *         *         *         *         *         *         *         *             //Large Healing Potion
7174      0       80      *         *         *         *         *         *         *         *             //Large Mana Potion
7174      0       80      *         *         *         *         *         *         *         *             //Large Mana Potion
7174      0       80      *         *         *         *         *         *         *         *             //Large Mana Potion

//
12,003    0       0       49        *         12         21        21        *         *         900          //Wings of Spirit 
12,004    0       0       49        *         12         21        21        *         *         900          //Wings of Soul     
12,005    0       0       49        *         12         21        21        *         *         900          //Wings of Dragon
12,006    0       0       49        *         12         21        21        *         *         900          //Wings of Darkness
12,042    0       0       49        *         12         21        21        *         *         900          //Wings of Despair
12,049    0       0       49        *         12         21        21        *         *         900          //Cape of Fighter 
12,269    0       0       49        *         12         21        21        *         *         900          //Cloak of Limit
13,030    0       0       49        *         12         21        21        *         *         900          //Cape of Lord
//
00,024	  0       0       48 	    13        12         21        13        *         *         900 	       //Daybreak  
00,025    0       0       48 	    13        12         21        13        *         *         900 	       //Sword Dancer
00,033    0       0       48 	    13        12         21        13        *         *         900 	       //Holy Storm Claws
00,068    0       0       48 	    13        12         21        13        *         *         900 	       //Cookery Short
02,015    0       0       48 	    13        12         21        13        *         *         900 	       //Shining Scepter
02,029    0       0       48 	    *         12         21        13        *         *         900 	       //El Hazard Mace
03,016    0       0       48 	    13        12         21        13        *         *         900 	       //Vis Lance
04,022    0       0       48 	    13        12         21        13        *         *         900 	       //Albatross Bow
04,032    0       0       48 	    *         12         21        13        *         *         900 	       //Combat Quiver
05,013    0       0       48 	    *         12         21        13        *         *         900 	       //Platina Staff
05,018    0       0       48 	    *         12         21        13        *         *         900 	       //Demonic Stick
06,015    0       0       48 	    *         12         21        13        *         *         900 	       //Grand Soul Shield
06,016    0       0       48 	    *         12         21        13        *         *         900 	       //Elemental Shield
//
07,021    0       0       48        *         12         21        13        *         *         900         //Great Dragon Helm
08,021    0       0       48        *         12         21        13        *         *         900         //Great Dragon Armor
09,021    0       0       48        *         12         21        13        *         *         900         //Great Dragon Pants
10,021    0       0       48        *         12         21        13        *         *         900         //Great Dragon Gloves
11,021    0       0       48        *         12         21        13        *         *         900         //Great Dragon Boots
07,022    0       0       48        *         12         21        13        *         *         900         //Dark Soul Helm
08,022    0       0       48        *         12         21        13        *         *         900   	   //Dark Soul Armor
09,022    0       0       48        *         12         21        13        *         *         900   	   //Dark Soul Pants
10,022    0       0       48        *         12         21        13        *         *         900   	   //Dark Soul Gloves
11,022    0       0       48        *         12         21        13        *         *         900   	   //Dark Soul Boots
08,023    0       0       48        *         12         21        13        *         *         900   	   //Hurricane Armor
09,023    0       0       48        *         12         21        13        *         *         900   	   //Hurricane Pants
10,023    0       0       48        *         12         21        13        *         *         900   	   //Hurricane Gloves
11,023    0       0       48        *         12         21        13        *         *         900   	   //Hurricane Boots
07,024    0       0       48        *         12         21        13        *         *         900         //Red Spirit Helm
08,024    0       0       48        *         12         21        13        *         *         900         //Red Spirit Armor
09,024    0       0       48        *         12         21        13        *         *         900         //Red Spirit Pants
10,024    0       0       48        *         12         21        13        *         *         900         //Red Spirit Gloves
11,024    0       0       48        *         12         21        13        *         *         900         //Red Spirit Boots
07,028    0       0       48        *         12         21        13        *         *         900   	   //Dark Master Helm
08,028    0       0       48        *         12         21        13        *         *         900   	   //Dark Master Armor
09,028    0       0       48        *         12         21        13        *         *         900   	   //Dark Master Pants
10,028    0       0       48        *         12         21        13        *         *         900   	   //Dark Master Gloves
11,028    0       0       48        *         12         21        13        *         *         900   	   //Dark Master Boots
07,042    0       0       48        *         12         21        13        *         *         900   	   //Demonic Helm
08,042    0       0       48        *         12         21        13        *         *         900   	   //Demonic Armor
09,042    0       0       48        *         12         21        13        *         *         900   	   //Demonic Pants
10,042    0       0       48        *         12         21        13        *         *         900   	   //Demonic Gloves
11,042    0       0       48        *         12         21        13        *         *         900   	   //Demonic Boots
07,060    0       0       48        *         12         21        13        *         *         900         //Storm Jahad Helm
08,060    0       0       48        *         12         21        13        *         *         900         //Storm Jahad Armor
09,060    0       0       48        *         12         21        13        *         *         900         //Storm Jahad Pants
11,060    0       0       48        *         12         21        13        *         *         900         //Storm Jahad Boots
07,094    0       0       48        *         12         21        13        *         *         900         //Hirat Helm  
08,094    0       0       48        *         12         21        13        *         *         900         //Hirat Armor  
09,094    0       0       48        *         12         21        13        *         *         900         //Hirat Pants  
10,094    0       0       48        *         12         21        13        *         *         900         //Hirat Gloves  
11,094    0       0       48        *         12         21        13        *         *         900         //Hirat Boots
07,119    0       0       48 	    *         12         21        13        *         *         900         //Kanaz Helm
08,119    0       0       48 	    *         12         21        13        *         *         900   	   //Kanaz Armor
09,119    0       0       48 	    *         12         21        13        *         *         900   	   //Kanaz Pants
10,119    0       0       48 	    *         12         21        13        *         *         900   	   //Kanaz Gloves
11,119    0       0       48 	    *         12         21        13        *         *         900   	   //Kanaz Boots
07,176    0       0       48 	    *         12         21        13        *         *         900         //Slayer Demonic Helm
08,176    0       0       48 	    *         12         21        13        *         *         900   	   //Slayer Demonic Armor
09,176    0       0       48 	    *         12         21        13        *         *         900   	   //Slayer Demonic Pants
10,176    0       0       48 	    *         12         21        13        *         *         900   	   //Slayer Demonic Gloves
11,176    0       0       48 	    *         12         21        13        *         *         900   	   //Slayer Demonic Boots
13,003    0       0       48        *         *         *         *        *         *         900          //Horn of Dinorant
end